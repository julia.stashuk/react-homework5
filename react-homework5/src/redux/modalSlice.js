import { createSlice } from '@reduxjs/toolkit';

const modalSlice = createSlice({
  name: 'modal',
  initialState: {
    isOpen: false,
    modalType: null,
    modalId: null,
  },

  reducers: {
    openModal: (state, action) => {
      state.isOpen = true;
      state.modalType = action.payload.modalType;
      state.modalId = action.payload.id;
    },
    closeModal: (state) => {
      state.isOpen = false;
      state.modalType = null;
      state.modalId = null;; 
    },
  },
});

export const { openModal, closeModal } = modalSlice.actions;
export default modalSlice.reducer;