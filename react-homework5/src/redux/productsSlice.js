import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';


export const fetchProducts = createAsyncThunk('products/fetchProducts', async () => {
    const response = await fetch('/products.json');
    if (!response.ok) {
        throw new Error('Failed to fetch products');
    }
    const data = await response.json();
    return data;
});

const productsSlice = createSlice({
    name: 'products',
    initialState: {
        items: [],
        status: 'idle',
        error: null,
        cart: JSON.parse(localStorage.getItem('cart')) || []
    },
    reducers: {
      addToCart(state, action) {
        const { id } = action.payload;
        const existingCartItem = state.cart.find(item => item.id === id);
        if (existingCartItem) {
            existingCartItem.quantity += 1;
        } else {
            state.cart.push({ id, quantity: 1 });
        }
        localStorage.setItem('cart', JSON.stringify(state.cart));

    },
          decrementCartItem(state, action) {
            const { id } = action.payload;
            const item = state.cart.find(item => item.id === id);
            if (item && item.quantity > 1) {
              item.quantity -= 1;
            }
            localStorage.setItem('cart', JSON.stringify(state.cart))
          },
          incrementCartItem(state, action) {
            const { id } = action.payload;
            const item = state.cart.find(item => item.id === id);
            if (item) {
              item.quantity += 1;
            }
            localStorage.setItem('cart', JSON.stringify(state.cart))
          },
          removeFromCart(state, action) {
            const { id } = action.payload;
            state.cart = state.cart.filter(item => item.id !== id);
          },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchProducts.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchProducts.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.items = action.payload;
            })
            .addCase(fetchProducts.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            });
    }
});

export const { incrementCartItem, decrementCartItem, addToCart, removeFromCart } = productsSlice.actions;
export default productsSlice.reducer;
