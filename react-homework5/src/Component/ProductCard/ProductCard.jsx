import PropTypes from 'prop-types';
import "./ProductCard.scss"
import Button from '../Button/Button';
import SecondModal from '../SecondModal/SecondModal'
import React, { useState, useEffect } from 'react';
import StarImage from '../StarImage/StarImage'
import { useDispatch, useSelector } from 'react-redux';
import { openModal, closeModal } from '../../redux/modalSlice';
import { addToCart} from '../../redux/productsSlice';

const ProductCard = ({
    title = " ",
    id,
    price = " ",
    color = " ",
    image,
    handleFavourite, 
    isFavourite: initialIsFavourite = false,
}) => {
    const dispatch = useDispatch();
  const isModalOpen = useSelector((state) => state.modal.isOpen);
  const modalType = useSelector((state) => state.modal.modalType);
  const cart = useSelector((state) => state.products.cart);
  const modalId = useSelector((state) => state.modal.modalId);

    const [isFavourite, setIsFavourite] = useState(initialIsFavourite);

    useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
        setIsFavourite(favorites.includes(id));
    }, [id]);

    const isAddedToCart = cart.some(item => item.id === id);
    console.log('ProductCard - isAddedToCart result:', isAddedToCart);

    const handleStarClick =() => {
        handleFavourite(id)
        setIsFavourite(prevState => !prevState);
    };
    const handleAddToCartClick = () => {
        console.log('ProductCard - handleAddToCartClick:', id);
        dispatch(addToCart({ id }));
        dispatch(closeModal());
      };
   
    return (
        <div className="card">
            <div
                className={`svgWrapper ${isFavourite ? 'active' : ''}`}
                onClick={handleStarClick}
            >
                <StarImage isFavourite={isFavourite}/>
            </div>

            <img src={image} alt="product-image" className="image" />
            <h2 className="title">{title}</h2>
            
            <p className="price"> Price: {price} UAH</p>
            <span className="color"> {color}</span>

            <Button
            className={`main-button ${isAddedToCart ? 'added-to-cart' : ''}`}
            onClick={() => {
                console.log('isAddedToCart:', isAddedToCart);
                console.log('isModalOpen:', isModalOpen);
                if (!isAddedToCart && !isModalOpen) {
                  dispatch(openModal({modalType:"second", id}));
                }
              }}
            text={isAddedToCart ? "Added to Cart" : "Add to Cart"}
            />
         {isModalOpen && modalId === id && (
                <SecondModal
                    handleAddToCart={handleAddToCartClick}
                    handleCloseModal={() => dispatch(closeModal())}
                    title={title}
                    price={price}
                    id={id}
                />
            )}
        </div>
    );
};


ProductCard.propTypes = {
    title: PropTypes.string,
    id: PropTypes.number.isRequired,
    price: PropTypes.number,
    color: PropTypes.string,
    image: PropTypes.string,
    handleFavourite: PropTypes.func,
    isFavourite: PropTypes.bool,
};

export default ProductCard;
