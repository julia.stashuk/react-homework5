import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { useSelector } from 'react-redux';
import './CheckoutForm.scss';
import { NumericFormat } from 'react-number-format';

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required('First name is required').min(2, 'Last name must be at least 2 characters'),
  lastName: Yup.string().required('Last name is required').min(2, 'First name must be at least 2 characters'),
  age: Yup.number().required('Valid age of 18 or older is required').min(18, 'You must be 18 or older'),
  address: Yup.string().required('Address is required'),
  phone: Yup.string().required('Phone number is required'),
});

const CheckoutForm = () => {
  const cartProducts = useSelector((state) => state.products.cart);

  const handleCheckout = (values, { resetForm }) => {
    console.log('Form data:', values);
    console.log('Cart products:', cartProducts);
    localStorage.removeItem('cart');
    resetForm();
  };

  return (
    <div className="checkout-form">
      <h1 className="checkout-header">Order</h1>
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        age: '',
        address: '',
        phone: '',
      }}
      validationSchema={validationSchema}
      onSubmit={handleCheckout}
    >
      {({ touched, errors }) => (
        <Form>
          <div>
            <label htmlFor="firstName">First Name:</label>
            <Field
              type="text"
              id="firstName"
              name="firstName"
              placeholder="Write your first name"
              className={touched.firstName && errors.firstName ? 'error' : null}
            />
            <ErrorMessage name="firstName" component="div" className="error" />
          </div>
          <div>
            <label htmlFor="lastName">Last Name:</label>
            <Field
              type="text"
              id="lastName"
              name="lastName"
              placeholder="Write your last name"
              className={touched.lastName && errors.lastName ? 'error' : null}
            />
            <ErrorMessage name="lastName" component="div" className="error" />
          </div>
          <div>
            <label htmlFor="age">Age:</label>
            <Field
              type="number"
              id="age"
              name="age"
              placeholder="Write your age"
              className={touched.age && errors.age ? 'error' : null}
            />
            <ErrorMessage name="age" component="div" className="error" />
          </div>
          <div>
            <label htmlFor="address">Address:</label>
            <Field
              type="text"
              id="address"
              name="address"
              placeholder="Write your adress"
              className={touched.address && errors.address ? 'error' : null}
            />
            <ErrorMessage name="address" component="div" className="error" />
          </div>
          <div>
          <label htmlFor="phone">Phone:</label>
            <NumericFormat
              format="(###) ###-##-##"
              mask="_"
              id="phone"
              name="phone"
              placeholder="(###) ###-##-##"
              className={touched.phone && errors.phone ? 'error' : null}
              customInput={Field}
              onValueChange={(values) => {
                setFieldValue('phone', values.value);
              }}
            />
            <ErrorMessage name="phone" component="div" className="error" />
          </div>
          <button type="submit">Checkout</button>
        </Form>
      )}
    </Formik>
    </div>
  );
};

export default CheckoutForm;