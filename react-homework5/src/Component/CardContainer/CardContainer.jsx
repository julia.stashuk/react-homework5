import PropTypes from 'prop-types';
import React from 'react';
import ProductCard from '../ProductCard/ProductCard';
import "./CardContainer.scss";

const CardContainer = ({ products = [], handleAddToCart, handleRemoveFromCart, handleFavourite = () => { } }) => {

    return (
    <div className="card-container">
            {products && products.map((product) => {
            const { title, price, id, color, image, isFavourite, isAddedToCart } = product
                
               return <ProductCard key={id} title={title} price={price}
                    id={id} color={color} image={image} handleRemoveFromCart={handleRemoveFromCart}
                    isFavourite={isFavourite} handleFavourite={handleFavourite} 
                    handleAddToCart={handleAddToCart} isAddedToCart={isAddedToCart}
                    product={product}/>
            })}
    </div>
    
    )
}
CardContainer.propTypes = {
    products: PropTypes.array,
    handleFavourite:PropTypes.func,
    handleRemoveFromCart: PropTypes.func
};

export default CardContainer;
